var $ = global.jQuery = require('./node_modules/jquery');
window.$ = window.jQuery = module.exports;
const remote = require('electron').remote;

var doc = "Answers.txt";
$("#helperCurrentLocation").text(doc);

const electron = require('electron');
electron.ipcRenderer.on('home',(event, arg) => {
   goHome();
});
electron.ipcRenderer.on('nature',(event, arg) => {
    HowAreYouButton();
});
electron.ipcRenderer.on('save',(event, arg) => {
   fileSave();
});

function goHome(){
   $("#maingrid").css(
      {
         "position":"absolute",
         "display":"none"
      });
   $("#homegrid").css(
      {
         "position":"unset",
         "display":"grid"
      });
   $(".helper").css(
      {
         "position":"absolute",
         "display":"none"
      });
}

function Submit(){
    var remote = require('electron').remote;
    var fs = remote.require('fs');

    var d = new Date();
    var message = "-------------" +
        d.getFullYear() + "." +
        (d.getMonth() + 1) + "." +
        d.getDate() + " " +
        d.getHours() + ":" +
        d.getMinutes() + ":" +
        d.getSeconds() + "||" + $("#title").text() + "-------------\r\n";

    fs.appendFile(doc, message, (error) => {
        // In case of an error throw err exception. 
        if (error) throw err;
    });
    for (let i = 0; i < answers.length; i++) {
        fs.appendFile(doc, "Q: " + questions[i] + " A: " + answers[i] + "\r\n", (error) => {
            if (error) throw err;
        });
    }
}