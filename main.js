const { app, BrowserWindow, Menu } = require('electron');
const path = require('path');
const url = require('url');
let mainWindow = null;

app.once('ready',() => {
    mainWindow = new BrowserWindow({
        width: 600,
        height: 400,
        webPreferences: {
          //preload: path.join(__dirname, 'preload.js'),
          nodeIntegration: true,
          contextIsolation: false,
          enableRemoteModule: true
        }
    });

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    mainWindow.once('ready-to-show', () => {
        mainWindow.show();
    });

    const mainPage = Menu.buildFromTemplate(mainPageTemplate);

    Menu.setApplicationMenu(mainPage);
    //mainWindow.setResizable(false);
})

//create menu template
const mainPageTemplate = [
    {
        label:'File',
        submenu:[
            {
                label: 'Home',
                accelerator: process.platform == 'darwin' ? 'Command+0' : 'Ctrl+0',
                click: () => {
                    mainWindow.webContents.send('home')
                }
            },
            {
                label: 'Topic',
                submenu:[
                    {
                        label: 'Nature',
                        click: () => {
                            mainWindow.webContents.send('nature')
                        }
                    },
                    {
                        label: 'Pseudo science',
                        click: () => {
                            mainWindow.webContents.send('science')
                        }
                    }
                ]
            },
            {
                label: '💾 Save Location',
                accelerator: process.platform == 'darwin' ? 'Command+S' : 'Ctrl+S',
                click: () => {
                    mainWindow.webContents.send('save')
                }
            },
            {
                label: '🚪 Quit',
                accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
                click(){
                    app.quit();
                }
            }
        ]
    },
    {
        label:'Help',
        accelerator: process.platform == 'darwin' ? 'Command+H' : 'Ctrl+H',
        click: () => {
            mainWindow.webContents.send('help')
        }
    }
];